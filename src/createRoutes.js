import express from 'express';
//import * as entries from './entriesHandler.js'
//import * as users from './usersHandler.js'
import { v4 as uuidv4 } from 'uuid';
import * as hash from './lib/hash.js'
import emailValidator from 'email-validator'
import * as db from './lib/database.js'

const router = express.Router();

//// Create entry when user submits contact form
router.post('/contactFormEntries', async (req, res, next) => {
  try {
    const body = req.body;
    const createUserID = uuidv4();
    const sql = 'INSERT INTO contactFormEntries (id, name, email, phoneNumber, content) VALUES (?,?,?,?,?)';
    const param = [createUserID, body.name, body.email, body.phoneNumber, body.content]
    // Bad Request: missing or invalid
    const invalid = [];
    if (body.name == null || (Object.values(body.name)).length == 0){
      invalid.push("name")
    }
    if (body.email == null || emailValidator.validate(body.email) == false) {
      invalid.push("email")
    }
    if (body.phoneNumber == null || (Object.values(body.phoneNumber)).length == 0){
      invalid.push("phoneNumber")
    }
    if (body.content == null || (Object.values(body.content)).length == 0) {
      invalid.push("content")
    }
    if (invalid.length > 0) {
        return res.status(400).send({message: "validation error", invalid})
    }
    // Create: Send to Database
    await db.query(sql, param);
    return res.status(201).send({message: 'Contact inquiry sent'})

    /* Create and Log
    const newEntryLog = []
    const newEntry = (id, name, email, phoneNumber, content) => {
      entries.add({id, name, email, phoneNumber, content})
      newEntryLog.push({id, name, email, phoneNumber, content})
    }
    newEntry(uuidv4(), body.name, body.email, body.phoneNumber, body.content);
    return res.status(201).send({message: 'Contact inquiry sent'})
    */

  } catch (err) {
      console.error(err)
      return next(err)
  }
})

//// Create a user
router.post('/users', async (req, res, next) => {
  try {
    const body = req.body;
    // Bad Request: missing or invalid
    const invalid = [];
    if (body.name == null || (Object.values(body.name)).length == 0){
      invalid.push("name")
    }
    if (body.password == null || (Object.values(body.password)).length < 8) {
      invalid.push("password")
    }
    if (body.email == null || emailValidator.validate(body.email) == false) {
      invalid.push("email")
    }
    if (invalid.length > 0) {
      return res.status(400).send({message: "validation error", invalid})
    }
    // Conflict: email address already exists in database
    let sql = 'SELECT * FROM users WHERE email =?'
    const database = await db.query(sql, body.email);
    if (database.length > 0) {
      return res.status(409).send({message: `${body.email} already exists in the database`})
    }
    //// Create
    // Run function createHash
    const newHash = await hash.createHash(body.password)
    // Generate a user ID
    const createUserID = uuidv4();
    // Submit new user to database
    sql = 'INSERT INTO users (id, name, password, email) VALUES (?,?,?,?)';
    const param = [createUserID, body.name, newHash, body.email]
    await db.query(sql, param);
    return res.status(201).send({message: 'New user created'})

  } catch (err) {
      console.error(err)
      return next(err)
  }
})


export default router;
