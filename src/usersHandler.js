import { promises as fs } from 'fs'
import path from 'path'

const file = path.resolve(process.env.DB_USERS_FILE_LOCATION)

// Internal function
const writeFile = async (newDatabase) => {
  await fs.writeFile(file, JSON.stringify(newDatabase))
}

// Exported functions
const getAll = async () => {
   try {
     return JSON.parse(await fs.readFile(file))
   } catch (err) {
      if (err.code === 'ENOENT') {
        const array = []
        writeFile(array);
        return getAll()
      }
      console.error(err)
      throw err
   }
}

const add = async (newData) => {
  try {
    let database = await getAll()
    database.push(newData)
    await writeFile(database)
  } catch (err) {
      console.error(err)
      throw err
  }
}

export {
  getAll,
  add
}
