import express from 'express';
import jwtVerify from './lib/jwtVerify.js';
import * as db from './lib/database.js'

const router = express.Router();

// JWT Verify Middleware
router.use(jwtVerify)

//// Get
router.get('/resume', async (req, res, next) => {
  try {
    const sql = 'SELECT * FROM resume ORDER BY id DESC';
    const database = await db.query(sql, req.params);
    // Ok: list all entries
    return res.status(200).send(database);
  } catch (err) {
      console.error(err)
      return next(err)
  }
})

//// Get by ID
router.get('/resume/:id', async (req, res, next) => {
  try {
    const sql = 'SELECT * FROM resume WHERE id =?';
    const database = await db.query(sql, req.params.id);
    // Not Found: requested id not in database
    if (database.length == 0) {
      return res.status(404).send({message: `Resume item id: ${req.params.id} not found`})
    }
    // Ok: list all entries
    return res.status(200).send(database);
  } catch (err) {
      console.error(err)
      return next(err)
  }
})

//// Delete
router.delete('/resume/:id', async (req, res, next) => {
  try {
    const body = req.body;
    // Bad Request: Invalid ID
    if (body.id != req.params.id) {
      return res.status(400).send({message: "Invalid ID"})
    }
    let sql = 'SELECT * FROM resume WHERE id = ?';
    const database = await db.query(sql, req.params.id);
    // Not Found: requested id not in database
    if (database.length == 0) {
      return res.status(404).send({message: `Resume item id: ${req.params.id} not found`});
    }
    // Ok: Delete item
    sql = 'DELETE FROM resume WHERE id = ?';
    await db.query(sql, req.params.id);
    return res.status(200).send({message: "Resume item has been deleted"})
  } catch (err) {
    console.error(err)
    return next(err)
  }
})

//// Post
router.post('/resume', async (req, res, next) => {
  try {
    const body = req.body;
    // Bad Request: missing or invalid
    const invalid = [];
    if (body.category == null || (Object.values(body.category)).length == 0){
      invalid.push("category")
    }
    if (body.location == null || (Object.values(body.location)).length == 0){
      invalid.push("location")
    }
    if (body.title == null || (Object.values(body.title)).length == 0) {
      invalid.push("title")
    }
    if (body.description == null || (Object.values(body.description)).length == 0) {
      invalid.push("description")
    }
    if (body.startDate == null || (Object.values(body.startDate)).length == 0) {
      invalid.push("startDate")
    }
    if (invalid.length > 0) {
        return res.status(400).send({message: "validation error", invalid})
    }
    // Create: Send to Database
    const sql = 'INSERT INTO resume (category, location, title, description, startDate, endDate) VALUES (?,?,?,?,?,?)';
    const param = [body.category, body.location, body.title, body.description, body.startDate, body.endDate]
    await db.query(sql, param);
    return res.status(201).send({message: "Resume item posted"})
  } catch (err) {
      console.error(err)
      return next(err)
  }
})

//// Patch
router.patch('/resume/:id', async (req, res, next) => {
  try {
    const body = req.body;
    // Bad Request: Invalid ID
    if (body.id != req.params.id) {
      return res.status(400).send({message: "Invalid ID"})
    }
    // Bad Request: missing or invalid
    const invalid = [];
    if (body.category == null || (Object.values(body.category)).length == 0){
      invalid.push("category")
    }
    if (body.location == null || (Object.values(body.location)).length == 0){
      invalid.push("location")
    }
    if (body.title == null || (Object.values(body.title)).length == 0) {
      invalid.push("title")
    }
    if (body.description == null || (Object.values(body.description)).length == 0) {
      invalid.push("description")
    }
    if (body.startDate == null || (Object.values(body.startDate)).length == 0) {
      invalid.push("startDate")
    }
    if (invalid.length > 0) {
        return res.status(400).send({message: "validation error", invalid})
    }
    let sql = 'SELECT * FROM resume WHERE id = ?';
    const database = await db.query(sql, req.params.id);
    // Not Found: requested id not in database
    if (database.length == 0) {
      return res.status(404).send({message: `Resume item ${req.params.id} not found`});
    }
    // Create: Send to Database
    sql = 'UPDATE resume SET category = ?, location = ?, title = ?, description = ?, startDate = ?, endDate = ? WHERE id = ?';
    const param = [body.category, body.location, body.title, body.description, body.startDate, body.endDate, body.id]
    await db.query(sql, param);
    return res.status(201).send({message: "Resume item updated"})
  } catch (err) {
      console.error(err)
      return next(err)
  }
})


export default router;

