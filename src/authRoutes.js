import express from 'express';
//import * as entries from './entriesHandler.js'
//import * as users from './usersHandler.js'
import * as jwtGenerator from 'jsonwebtoken';
import jwtVerify from './lib/jwtVerify.js';
import * as hash from './lib/hash.js'
import * as db from './lib/database.js'

const router = express.Router();

//// Log a registered user in (create JWT, return the token)
router.post('/auth', async (req, res, next) => {
  try {
    // Unauthorized: email and password do not match (never specify which is incorrect)
    const body = req.body;
    const unauthorized = () => res.status(401).send({message: "incorrect credentials provided"})
    // Find email in database
    const sql = 'SELECT * FROM users WHERE email = ?';
    const database = await db.query(sql, body.email)

    //const database = await users.getAll();
    const findUser = database.filter(user => user.email == body.email)
    // if email not found
    if (findUser.length == 0) {
      unauthorized()
    }
    // Run the verifyHash function
    const newVerify = await hash.verifyHash(findUser[0].password, body.password)
    // Compare the hashes
    if (newVerify == false) {
      unauthorized()
    }
    // Accepted
    // Issue Toekn
    const email = body.email
    const token = jwtGenerator.sign({email}, process.env.JWT_SECRET, {expiresIn: '4h'})
    return res.status(202).send({token})
  } catch (err) {
      console.error(err)
      return next(err)
  }
})

// JWT Verify Middleware
router.use(jwtVerify)

//// Get listing of all form entries (when authorized with JWT)
router.get('/contactFormEntries', async (req, res, next) => {
  try {
    const sql = 'SELECT * FROM contactFormEntries ORDER BY date DESC';
    const database = await db.query(sql, req.params);
    // Ok: list all entries
    return res.status(200).send(database);
  } catch (err) {
      console.error(err)
      return next(err)
  }
})

//// Get a specific form entry (when authorized with JWT) ***UPDATE to connect to DBeaver later ****
router.get('/contactFormEntries/:id', async (req, res, next) => {
  try {
    // Not Found: requested id not in database
    const database = await entries.getAll();
    const findEntry = database.filter(entry => entry.id == req.params.id)
      if (findEntry.length == 0) {
        return res.status(404).send({message: `entry ${req.params.id} not found`})
      }
    // Ok: list specific entry
    return res.status(200).send(findEntry[0]);
  } catch (err) {
      console.error(err)
      return next(err)
  }
})


export default router;
