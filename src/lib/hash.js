import argon2 from 'argon2';

// Create a hashed password
const createHash = async (password) => {
  try {
    const hash = await argon2.hash(password)
    return hash
  } catch (err) {
    console.error(err)
  }
}

// Verify a hashed password
const verifyHash = async (hash, password) => {
  try {
    const match = await argon2.verify(hash, password);
    return match;
  } catch (err) {
    console.error(err)
  }
}

export {
  createHash,
  verifyHash
}
