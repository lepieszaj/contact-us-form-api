import mysql from 'mysql'

// create connection to database
const db = mysql.createConnection ({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  port: process.env.DB_PORT
});

// connect to database
db.connect((err) => {
  if (err) {
      throw err;
  }
  console.log('Connected to database');
});


exports.query = (sql, param) => {
  return new Promise((resolve, reject) => {
      db.query(sql, param, (err, res) => {
          if (err) reject(err);
          else resolve(res);
      })
  })
}
