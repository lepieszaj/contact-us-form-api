import * as jwt from 'jsonwebtoken'

export default (req, res, next) => {
    const authHeader = req.headers['authorization']
    // Value of the authorization header is typically: "Bearer JWT", hence splitting with empty space and getting second part of the split
    const token = authHeader && authHeader.split(' ')[1]
    try {
        const data = jwt.verify(token, process.env.JWT_SECRET)
        req.user = data
        next()
    } catch (err) {
        console.error(err)
        if (err.message == 'jwt must be provided') {
          return res.status(403).send({message: "token not provided"})
        } if (err.message == 'invalid signature') {
            return res.status(403).send({message: "invalid token"})
        } if (err.message == 'jwt expired') {
            return res.status(403).send({message: "token expired"})
        } return next(err)
    }
}
