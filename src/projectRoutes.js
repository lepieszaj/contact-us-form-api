import express from 'express';
import jwtVerify from './lib/jwtVerify.js';
import * as db from './lib/database.js'

const router = express.Router();

// JWT Verify Middleware
router.use(jwtVerify)

//// Get
router.get('/projects', async (req, res, next) => {
  try {
    const sql = 'SELECT * FROM projects ORDER BY id DESC';
    const database = await db.query(sql, req.params);
    // Ok: list all entries
    return res.status(200).send(database);
  } catch (err) {
      console.error(err)
      return next(err)
  }
})

//// Get by ID
router.get('/projects/:id', async (req, res, next) => {
  try {
    const sql = 'SELECT * FROM projects WHERE id =?';
    const database = await db.query(sql, req.params.id);
    // Not Found: requested id not in database
    if (database.length == 0) {
      return res.status(404).send({message: `Project id: ${req.params.id} not found`})
    }
    // Ok: list all entries
    return res.status(200).send(database);
  } catch (err) {
      console.error(err)
      return next(err)
  }
})

//// Delete
router.delete('/projects/:id', async (req, res, next) => {
  try {
    const body = req.body;
    // Bad Request: Invalid ID
    if (body.id != req.params.id) {
      return res.status(400).send({message: "Invalid ID"})
    }
    let sql = 'SELECT * FROM projects WHERE id = ?';
    const database = await db.query(sql, req.params.id);
    // Not Found: requested id not in database
    if (database.length == 0) {
      return res.status(404).send({message: `Project ${req.params.id} not found`});
    }
    // Ok: Delete item
    sql = 'DELETE FROM projects WHERE id = ?';
    await db.query(sql, req.params.id);
    return res.status(200).send({message: "Project has been deleted"})
  } catch (err) {
    console.error(err)
    return next(err)
  }
})

//// Post
router.post('/projects', async (req, res, next) => {
  try {
    const body = req.body;
    // Bad Request: missing or invalid
    const invalid = [];
    if (body.title == null || (Object.values(body.title)).length == 0){
      invalid.push("title")
    }
    if (body.description == null || (Object.values(body.description)).length == 0){
      invalid.push("description")
    }
    if (body.link == null || (Object.values(body.link)).length == 0) {
      invalid.push("link")
    }
    if (body.image == null || (Object.values(body.image)).length == 0) {
      invalid.push("image")
    }
    if (invalid.length > 0) {
        return res.status(400).send({message: "validation error", invalid})
    }
    // Create: Send to Database
    const sql = 'INSERT INTO projects (title, keyword1, keyword2, keyword3, description, link, image) VALUES (?,?,?,?,?,?,?)';
    const param = [body.title, body.keyword1, body.keyword2, body.keyword3, body.description, body.link, body.image]
    await db.query(sql, param);
    return res.status(201).send({message: "Project posted"})
  } catch (err) {
      console.error(err)
      return next(err)
  }
})

//// Patch
router.patch('/projects/:id', async (req, res, next) => {
  try {
    const body = req.body;
    // Bad Request: Invalid ID
    if (body.id != req.params.id) {
      return res.status(400).send({message: "Invalid ID"})
    }
    // Bad Request: missing or invalid
    const invalid = [];
    if (body.title == null || (Object.values(body.title)).length == 0){
      invalid.push("title")
    }
    if (body.description == null || (Object.values(body.description)).length == 0){
      invalid.push("description")
    }
    if (body.link == null || (Object.values(body.link)).length == 0) {
      invalid.push("link")
    }
    if (body.image == null || (Object.values(body.image)).length == 0) {
      invalid.push("image")
    }
    if (invalid.length > 0) {
        return res.status(400).send({message: "validation error", invalid})
    }
    let sql = 'SELECT * FROM projects WHERE id = ?';
    const database = await db.query(sql, req.params.id);
    // Not Found: requested id not in database
    if (database.length == 0) {
      return res.status(404).send({message: `Project ${req.params.id} not found`});
    }
    // Create: Send to Database
    sql = 'UPDATE projects SET title = ?, keyword1 = ?, keyword2 = ?, keyword3 = ?, description = ?, link = ?, image = ? WHERE id = ?';
    const param = [body.title, body.keyword1, body.keyword2, body.keyword3, body.description, body.link, body.image, body.id]
    await db.query(sql, param);
    return res.status(201).send({message: "Project updated"})
  } catch (err) {
      console.error(err)
      return next(err)
  }
})


export default router;
