# Getting Started

Install:
- Run "npm install" to install all dependencies.

Configure the Environment:
- Copy the file .env.example and rename it to .env
- Add a secret to JWT_SECRET= (use a password generator to generate 64 characters or so)

Run the API:
- Run `npm start` or `npm run dev` to activate nodemon.
OR
- Run the docker containers `docker-compose up --build`

# Set up the Database

- Connect to the database on port 3307.
- For testing, the root password is set to: root_pass
- Run the mysql scripts below.

```
use joannaPortfolio;

CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  primary key (`id`)
);

CREATE TABLE IF NOT EXISTS `contactFormEntries` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phoneNumber` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `date` timestamp NOT null default current_timestamp on update current_timestamp,
  primary key (`id`)
);

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `keyword1` varchar(255) NULL,
  `keyword2` varchar(255) NULL,
  `keyword3` varchar(255) NULL,
  `description` text NOT NULL,
  `link` text NOT NULL,
  `image` varchar(255) NOT NULL,
  primary key (`id`)
);

CREATE TABLE IF NOT EXISTS `resume` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `location` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  primary key (`id`)
);

```

Use Nodemon to create a user account (so the app can hash your password before inserting into the database):

POST
Location: http://localhost:8080/users
Body:
{
    "name": "ENTER NAME",
    "password": "ENTER PASSWORD",
    "email": "ENTER EMAIL"
}

# Run the front end
https://github.com/jlepi/portfolio-react

# To Deploy
- Change ${} values in app.yaml
- Run `gcloud app deploy`
