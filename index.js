import express from 'express'
import createRoutes from './src/createRoutes.js'
import authRoutes from './src/authRoutes.js'
import projectRoutes from './src/projectRoutes.js'
import resumeRoutes from './src/resumeRoutes.js'
import cors from 'cors'

const PORT = process.env.APP_PORT || 4000;
const app = express();

app.use(express.json());

app.use(cors());

// Middleware
app.use((req, res, next) => {
  // global before middleware
  console.log(`Requested URI: ${req.originalUrl}`)
  next()
})

//Middleware
app.use((req, res, next) => {
  // global after midleware
  next()
  console.log(`Finished request: ${req.originalUrl}`)
})

app.use('/', createRoutes, authRoutes, projectRoutes, resumeRoutes);

// 404 Not Found Handler
app.use((req, res, next) => {
  return res.status(404).send({message: "not found"})
})

// Global Error Handler
app.use((err, req, res, next) => {
  if (res.headersSent) {
      return next(err)
  }
  console.error(err.stack)
  return res.status(500).send({error: "Unexpected error found"})
})

app.listen(PORT, () => console.log(`API server ready on http://localhost:${PORT}`));
